<!DOCTYPE html>
<html>
<head profile="http://www.w3.org/2005/10/profile">
    <title>Insurance Institute for Highway Safety: Road Regulation Research</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="/css/style.css?v2">
    <link rel="stylesheet" type="text/css" href="/css/roboto.css">
    <link rel="icon" type="image/png" href="/images/favicon-32.png"  sizes="32x32"  >
    <link rel="icon" type="image/png" href="/images/favicon-48.png"  sizes="48x48"  >
    <link rel="icon" type="image/png" href="/images/favicon-96.png"  sizes="96x96"  >
    <link rel="icon" type="image/png" href="/images/favicon-144.png" sizes="144x144">
</head>

<body id="iihs-road-safety-regulation-research">

<nav>
<section>
    <span class="home">
        <a href="/">
            <img src="/images/favicon-144.png" class="header">
            <span class="small-screen">Home</span>
            <span class="big-screen">I Can't Believe It's Not Government!</span>
        </a>
    </span>
    <span class="links">
        <a href="/contact/">Contact</a>
        <a href="/about/">About</a>
    </span>
</section>
</nav>

<main>

<article class='story'>
<h4>
    <a href="/">Home</a> ⇒
    <a href="/regulation">Regulation</a> ⇒
    <a href="/regulation/ground-traffic-regulation">Ground Traffic Regulation</a> ⇒
</h4>



<h1><a href="/">Insurance Institute for Highway Safety: Road Regulation Research</a></h1>

<p>Governments set guidelines for road rules and management to improve safety for drivers. In order to continually optimize rules, government agencies such as the <a href="https://en.wikipedia.org/wiki/National_Highway_Traffic_Safety_Administration">National Highway Traffic Safety Administration</a> in the United States perform research. Would a private organization have the incentive to perform similar research?</p>
<p>The <a href="http://www.iihs.org/">Insurance Institute for Highway Safety</a>, or IIHS, is a <a href="https://www.iihs.org/iihs/about-us">nonprofit organization</a> started in 1959. They are funded entirely by a <a href="https://www.iihs.org/iihs/about-us/member-groups">large number</a> of auto insurance companies and associations.  It is primarily known as an organization that <a href="/regulation/product-safety/iihs-crash-testing-cars">performs crash tests on automobiles</a>. However, they have also been involved in researching road hazards, and providing policy recommendations to the government. The Highway Loss Data Institute, or <a href="http://www.iihs.org/iihs/about-us/hldi">HLDI</a>, is a related organization.</p>
<div class="toc"><span class="toctitle">Contents</span><ul>
<li><a href="#relevance-to-voluntary-regulation">Relevance to voluntary regulation</a></li>
<li><a href="#research-and-policy-influence">Research and Policy Influence</a></li>
<li><a href="#incentives">Incentives</a></li>
<li><a href="#what-remains">What remains</a></li>
<li><a href="#sources">Sources</a><ul>
<li><a href="#references">References</a></li>
<li><a href="#opinion-statements">Opinion Statements</a></li>
<li><a href="#other-resources">Other Resources</a></li>
<li><a href="#see-also">See Also</a></li>
</ul>
</li>
</ul>
</div>
<h2 id="relevance-to-voluntary-regulation">Relevance to voluntary regulation</h2>
<p>Obviously, road regulation via legislation requires using government force, and the IIHS is ultimately complicit in collaborating with the government to enforce their vision. Thus, this is not a story about free market achievement from start to finish. However, what this does demonstrate is that insurance companies have the incentive to be a large scale coordinating force, by researching traffic rules, whatever the enforcement mechanism may be. And considering their incentives, it can be argued that they are interested in promoting <em>effective</em> rules.</p>
<h2 id="research-and-policy-influence">Research and Policy Influence</h2>
<p>IIHS conducts research on various topics related to driving safety, such as cell phone use, <a href="http://www.iihs.org/iihs/topics/t/Roadway%20and%20environment/bibliography/bytag">animal hazards</a>, <a href="http://www.iihs.org/iihs/topics/t/Roundabouts/bibliography/bytag">roundabouts</a>, and traffic lights. Some of this research leads to the IIHS pushing for government policy changes.</p>
<p>Specific <a href="http://www.iihs.org/iihs/about-us/milestones">examples</a> of IIHS/HLDI research include:</p>
<ul>
<li>Determining that longer yellow lights were safer, which lead to a change in legislation.</li>
<li>Determining that right turns on red lights were dangerous, leading NYC policymakers to restrict it.</li>
<li>Determining that daylight savings reduces crash deaths, influencing legislation to lengthen it.</li>
<li>Research on teen drivers influencing licensing laws in Florida.</li>
<li>Determining low speed vehicles pose a safety threat.</li>
<li>Determining that cellphone and texting bans do not reduce crashes.</li>
<li>Determining red light cameras significantly reduce fatal crashes, and pushed for red light cameras to be implemented.</li>
</ul>
<h2 id="incentives">Incentives</h2>
<p><a href="http://blogs.wsj.com/numbers/seeing-red-1208/">Some argue</a> that, while the IIHS and HLDI are interested in accurately assessing the safety of cars, they are also interested in having tickets issued to drivers, because it increases their premiums in some cases. As such, they may have an ulterior motive in promoting the use of red light cameras.</p>
<p>Perhaps they have such a perverse incentive when it comes to ticketable offenses, but it seems unlikely to apply to increasing yellow light duration, arguing <em>against</em> the usefulness of banning cell phones, and influencing daylight saving time.</p>
<h2 id="what-remains">What remains</h2>
<p>The insurance companies are not, alone, regulating the road. The are very much doing so with the help of the government. The government still enforces the rules, and is responsible for normalizing rules across large regions, to keep them consistent for drivers. And, as it is, IIHS and HLDI probably produce much less of the body of research that goes into traffic safety, than does the government.</p>
<p>However, in a world without government roads, there would be more place for the insurance companies to research these regulations. The examples above point to the fact that insurance companies do have the incentive to do so, <em>despite</em> the government already conducting its own research. Without government researching <em>anything</em>, insurance companies would have much more incentive to conduct research.</p>
<p>So what about government enforcement? In a more libertarian world, where roads are privately owned and managed, insurance companies could still have an influence. Drivers want to know what roads to take. Insurance companies could give safety ratings for the roads, just as they do for vehicles. They could offer discounts on premiums to drivers for avoiding roads with low ratings. Just as they have an incentive to accurately measure the safety of car features and specific cars, they have an incentive to accurately measure the safety of road features and specific roads. And remember that, though these policy recommendations are currently enforced by the government, it is still possible for them to be enforced as rules of use of a private road.</p>
<h2 id="sources">Sources</h2>
<h3 id="references">References</h3>
<ul>
<li><a href="http://www.iihs.org/">IIHS Main Website</a></li>
<li><a href="https://www.iihs.org/iihs/about-us">IIHS About Page</a></li>
<li><a href="https://www.iihs.org/iihs/about-us/member-groups">IIHS Membership and Funding</a></li>
<li><a href="http://www.iihs.org/iihs/about-us/milestones">IIHS Milestones</a></li>
<li>IIHS <a href="http://www.iihs.org/iihs/topics/t/Roundabouts/bibliography/bytag">Roundabout</a> research and information</li>
<li>IIHS <a href="http://www.iihs.org/iihs/topics/t/Roadway%20and%20environment/bibliography/bytag">Roadway and Environment</a> research and information</li>
<li><a href="http://www.iihs.org/iihs/about-us/hldi">About HLDI</a></li>
</ul>
<h3 id="opinion-statements">Opinion Statements</h3>
<ul>
<li><a href="http://blogs.wsj.com/numbers/seeing-red-1208/">New York Times blog post</a> about the red light camera controversy.</li>
</ul>
<h3 id="other-resources">Other Resources</h3>
<ul>
<li><a href="http://en.wikipedia.org/wiki/Insurance_Institute_for_Highway_Safety">Wikipedia article</a></li>
</ul>
<h3 id="see-also">See Also</h3>
<ul>
<li><a href="/regulation/product-safety/iihs-crash-testing-cars">IIHS and crash testing</a></li>
</ul>
</article>

</main>

<footer>
<section>
<p>
    <a href="https://twitter.com/itsnotgov">Twitter</a>
    <a rel="me" href="https://liberdon.com/@itsnotgov">Liberdon (Mastodon)</a>
    <a href="https://gitlab.com/orblivion/itsnotgov">Source</a>
    <a href="/terms-of-service">Terms of Service</a>
    <a href="/privacy-policy">Privacy Policy</a>
</p>
</section>
</footer>



</body>
</html>