<!-- title: About -->
<!-- render: yes -->
# About

This site collects stories from a free society to show what can be done on a voluntary basis. The aim is to take an intellectually honest approach so we know what works and what doesn't, what we know how to do and what we don't know how to do - <em>yet</em>. Stories are organized by "government function".

This site is an open source project ([contributions]({{ source_link }}) welcome) (based originally on a small but very useful project called [makesite.py][makesite]). Contribution instructions coming soon. For now take a look at our (work-in-progress) [story writing guidelines][writing_guidelines] and [story submission criteria][submission_criteria] for an idea of what we're looking for. Content is licensed as [CC0][cc0]. Code, fonts, and possibly other things are licensed differently. See [here]({{ source_link }}) for details.

[cc0]: https://creativecommons.org/publicdomain/zero/1.0/
[makesite]: https://github.com/sunainapai/makesite/
[writing_guidelines]: /help/story-writing-guidelines
[submission_criteria]: /help/story-submission-criteria
