<!-- title: Smart Contracts -->
<!-- summary: Programmatically enforced contracts that require no trusted third party (government or otherwise) to enforce them -->

A [smart contract][investopedia_smart_contracts] is a contract encoded in software which automatically enforces its terms. They are generally [implemented as][investopedia_smart_contracts] part of a blockchain platform, such as [Ethereum][wikipedia_ethereum], removing the requirement to trust any third parties in the transaction. Per a [United States Senate report][senate_report] about blockchain technology:

> While smart contracts might sound new, the concept is rooted in basic contract law. Usually, the judicial system adjudicates contractual disputes and enforces terms, but it is also common to have another arbitration method, especially for international transactions. With smart contracts, a program enforces the contract built into the code.

# Results

## Useful Examples

![Cryptocurrencies Connected](/images/stories/legal-system/contract-enforcement/smart-contracts/currencies.jpg)

It is still a fairly new concept (as of 2019), however there are at least some examples of it working in practice. For example, [decentralized cryptocurrency exchanges][yahoo_finance] are one solution to issues of trust inherent in centralized exchanges run by companies. Rather than using a company to act as a trusted third party, the trades [happen programmatically][yahoo_finance] via a smart contract.

## Limitations

A remaining challenge for smart contracts is to connect them to the real world, rather than merely being in terms of tokens and interactions with blockchains. It [is possible][business_oracles] to obtain information from the outside world in a distributed manner with a feature referred to as an "oracle". However, a currency exchange supporting the exchange of US Dollars, for instance, [cannot be implemented][yahoo_finance] with smart contracts and other blockchain technology alone. There is a hypothetical idea of implementing special computer chips to be used in physical products to facilitate ownership transfer via smart contracts, but that [has not yet][investopedia_understanding_smart_contracts] been implemented.

One major risk of smart contracts is the fact that a mistake in programming can be effectively treated as a [legal loophole][cbc] that would lead to a result not intended by the contract's creators. One high profile incident lead to a party exploiting a flaw in a contract to [obtain millions of dollars][cbc] worth of Ether. This lead the developers of Ethereum to [create a "hard fork"][cbc] to erase the incident from Ethereum's ledger history. And indeed, the ability for the developers of the blockchain platform to rewrite history (and the political will to do so in extreme cases, given the cooperation of the blockchain's user base) does pose another potential threat to the integrity of smart contracts.

# Sources

## References

[cbc]: https://www.cbc.ca/news/technology/ethereum-hack-blockchain-fork-bitcoin-1.3719009 (CBC - How a $64M hack changed the fate of Ethereum, Bitcoin's closest competitor)
* [How a $64M hack changed the fate of Ethereum, Bitcoin's closest competitor][cbc] - Canadian Broadcasting Corporation
[senate_report]: https://www.jec.senate.gov/public/_cache/files/aaac3a69-e9fb-45b6-be9f-b1fd96dd738b/chapter-9-building-a-secure-future-one-blockchain-at-a-time.pdf (U.S. Senate Report on Blockchain Technology)
* A [U.S. Senate Report][senate_report] related to blockchain technology
* Investopedia
[investopedia_smart_contracts]: https://www.investopedia.com/terms/s/smart-contracts.asp (Investopedia - Smart Contracts)
    * [Smart Contracts][investopedia_smart_contracts]
[investopedia_understanding_smart_contracts]: https://www.investopedia.com/news/understanding-smart-contracts/ (Investopedia - Understanding Smart Contracts)
    * [Understanding Smart Contracts][investopedia_understanding_smart_contracts]
[yahoo_finance]: https://finance.yahoo.com/news/decentralized-exchanges-stake-claim-cryptocurrency-221107642.html (Yahoo Finance - Decentralized Exchanges Stake Their Claim in the Cryptocurrency Ecosystem)
* [Decentralized Exchanges Stake Their Claim in the Cryptocurrency Ecosystem][yahoo_finance] - Yahoo Finance
[business_oracles]: https://www.business.com/articles/oracles-future-of-smart-contracts/ (Business - Oracles: The Future of Smart Contracts)
* [Oracles: The Future of Smart Contracts][business_oracles] - Business

## See Also

* Wikipedia
[wikipedia_smart_contract]: https://en.wikipedia.org/wiki/Smart_contract (Wikipedia - Smart Contract)
    * [Smart Contract][wikipedia_smart_contract]
[wikipedia_ethereum]: https://en.wikipedia.org/wiki/Ethereum (Wikipedia - Ethereum)
    * [Ethereum][wikipedia_ethereum]
[forbes_dexes]: https://www.forbes.com/sites/yoavvilner/2018/12/16/the-state-of-decentralized-exchanges-and-plans-for-2019/ (Forbes - The State Of Decentralized Exchanges And Plans For 2019)
* [The State Of Decentralized Exchanges And Plans For 2019][forbes_dexes] - Forbes
