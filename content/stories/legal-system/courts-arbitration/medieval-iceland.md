<!-- title: Medieval Iceland -->

<!-- redirect: /w/c/LegalSystem/CourtsArbitration/MedievalIceland -->

<!-- stub: We need to find reliable sources on this, which may be difficult. This seems to be a contentious topic, particularly between left and right libertarians. -->

# Sources

## Additional Resources

* [Wikipedia article section](http://en.wikipedia.org/wiki/Anarchism_in_Iceland#Medieval_Iceland)

## Further Reading

* [Mises.org article](http://mises.org/library/medieval-iceland-and-absence-government)
