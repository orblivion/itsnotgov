<!-- title: International Chamber Of Commerce: International Court of Arbitration -->

<!-- redirect: /w/c/LegalSystem/CourtsArbitration/international_chamber_of_commerce_international_co -->

<!-- stub: -->

The International Chamber of Commerce (ICC) is a large organization [founded in 1919][investopedia], and whose Court of Arbitration was [founded in 1923][investopedia]. They offer services to [both companies and governments][website_dispute_resolution] worldwide.

## Government Involvement

The International Chamber of Commerce has a relationship with the United Nations.

# Sources

## Reference

* [Investopedia Article][investopedia]
* Information on the ICC's website about their [relationship with the United Nations][website_un]

## Additional Resources

* Wikipedia:
  * [International Chamber of Commerce][wikipedia_icc]
  * [International Court of Arbitration][wikipedia_ica]
* Information on the ICC's website [about dispute resolution][website_dispute_resolution]
* [Interview][nrf_interview] of Secretary General of the ICC International Court of Arbitration at Norton Rose Fulbright

[investopedia]: https://www.investopedia.com/terms/i/international-chamber-of-commerce-icc.asp
[wikipedia_icc]: https://en.wikipedia.org/wiki/International_Chamber_of_Commerce
[wikipedia_ica]: https://en.wikipedia.org/wiki/International_Court_of_Arbitration
[website_dispute_resolution]: https://iccwbo.org/about-us/who-we-are/dispute-resolution/
[website_un]: https://iccwbo.org/global-issues-trends/global-governance/business-and-the-united-nations/
[nrf_interview]: http://www.nortonrosefulbright.com/knowledge/publications/127732/the-qa
[financier_worldwide]: https://www.financierworldwide.com/roundtable-international-arbitration-jun18/
