<!-- title: Bedford-Stuyvesant Volunteer Ambulance Corps (Brooklyn, New York)  -->

<!-- redirect: /w/c/EmergencyServices/EmergencyMedical/BSVAC -->

<!-- stub: -->

[Bedford-Stuyvesant Volunteer Ambulance Corps][official_website] (BSVAC) is a volunteer ambulance service located in the Brooklyn neighborhood of [Bedford Stuyvesant](https://en.wikipedia.org/wiki/Bedford%E2%80%93Stuyvesant,_Brooklyn) (aka Bed Stuy). Bed Stuy is a largely African American, low income neighborhood. It was started by James "Rocky" Robinson, who wanted to provide faster ambulance response times in his neighborhood, while providing job training to the neighborhood residents to reduce its crime rate.

# Funding

BSVAC is funded by [donations](http://www.bsvac.org/donate.html), [ambulance fees][vice_feature], and possibly some [government grants][official_website].

# Sources

* [Official Website][official_website]
* [Vice News video feature][vice_feature]

[official_website]: http://www.bsvac.org
[vice_feature]: https://www.youtube.com/watch?v=DBZO4BSB73M
