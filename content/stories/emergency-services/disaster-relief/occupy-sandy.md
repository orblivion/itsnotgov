<!-- title: Occupy Sandy -->

<!-- redirect: /w/c/EmergencyServices/DisasterRelief/OccupySandy -->

<!-- stub: -->

[Occupy Sandy](http://en.wikipedia.org/wiki/Occupy_Sandy) was a relief effort in response to [Hurricane Sandy](http://en.wikipedia.org/wiki/Hurricane_Sandy) that [grew out of the Occupy Wallstreet movement][time_article_working_with_govt]. They were known for [serving areas][nyt_article_fema_fell_short] neglected by [FEMA](http://en.wikipedia.org/wiki/Federal_Emergency_Management_Agency).

# Work with government

Some of Occupy Sandy's work was [in collaboration with][time_article_working_with_govt] the New York City government.

# Sources

## References

* [Time article describing Occupy Sandy working with government][time_article_working_with_govt]

## Other Resources

* [Wikipedia](http://en.wikipedia.org/wiki/Occupy_Sandy)

[time_article_working_with_govt]: http://nation.time.com/2012/11/13/best-of-enemies-why-occupy-activists-are-working-with-new-york-citys-government/
[nyt_article_fema_fell_short]: http://www.nytimes.com/2012/11/11/nyregion/where-fema-fell-short-occupy-sandy-was-there.html?pagewanted=all&_r=0
