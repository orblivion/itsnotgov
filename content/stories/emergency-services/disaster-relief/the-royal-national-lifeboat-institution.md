<!-- title: The Royal National Lifeboat Institution -->

<!-- redirect: /w/c/EmergencyServices/DisasterRelief/TheRoyalNationalLifeboatInstitution -->

<!-- stub: -->

The [Royal National Lifeboat Institution][wikipedia_article] is a charity sea rescue service that has operated in the United Kingdom since 1842. It recieves [less than 2%][official_website_government_funding] of its funding from any government. However, it does operate under a [Royal Charter](https://en.wikipedia.org/wiki/Royal_charter), giving it certain special legal authority.

# Sources

## Reference

* [Answer from FAQ][official_website_government_funding] of official website, regarding government funding.

## Other Resources
* [Official Website][official_website]
* [Wikipedia][wikipedia_article]

[official_website]: http://rnli.org
[wikipedia_article]: https://en.wikipedia.org/wiki/Royal_National_Lifeboat_Institution
[official_website_government_funding]: http://rnli.org/faqs/faqs/Pages/The-lifeboats/Do-you-receive-any-government-funding-for-your-services.aspx
