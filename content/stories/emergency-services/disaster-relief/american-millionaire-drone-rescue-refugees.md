<!-- title: Migrant Offshore Aid Station - Search and rescue in the Mediterranean with drones -->

<!-- redirect: /w/c/EmergencyServices/DisasterRelief/ThisAmericanMillionaireIsUsingDronesToRescu -->

<!-- stub: -->

# Sources

## Other Resources

* Gizmodo article: [This American Millionaire Is Using Drones To Rescue Migrants In Africa](http://indefinitelywild.gizmodo.com/this-american-millionaire-is-using-dronesto-rescue-migr-1689730355/)
* [Official Website](http://moas.eu/)
* [Wikipedia Article](http://en.wikipedia.org/wiki/Migrant_Offshore_Aid_Station)
