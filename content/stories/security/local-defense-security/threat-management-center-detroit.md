<!-- title: Threat Management Center (Detroit) -->

<!-- redirect: /w/c/Security/LocalDefenseSecurity/ThreatManagementCenterDetroit -->

<!-- stub: Need to add a cite a couple claims and clarify a couple statements. -->

# Background

The [Threat Management Center][official_website] (TMC) is a private security company located in Detroit, Michigan. Founded by Dale Brown in 1995, TMC performs many of the functions traditionally expected of a public police department. In Detroit, where police response times can (as of 2013) take as long as [58 minutes (if they accept your call)][independent_uk_article], having an alternative security force on-call has greatly reduced the incidence of violent crime.

The TMC's stated mission is: to make the world safer by denying the opportunity for violence to take place. 
Using the Violence Intervention Protective Emergency Response System (VIPERS) Threat Management emphasizes the use of deterrence, detection, and defense to achieve non-violent outcomes. This includes not using lethal force, or carrying lethal weapons. Their philosophy is based on using tactical psychology, tactical law, and tactical skills designed to de-escalate situations and which by design, are not conducive for violence.

# Incentives

TMC states that they receive enough income from providing security services for businesses and higher income individual clients, that they are able to [provide free protection][Huffington_Post_article] as a community service to people unable to afford their services. They also ask for [donations][official_website_financial_support].


# Results

After implementing the Violence Intervention Protective Emergency Response System (VIPERS)in 1995 in partnership with the Detroit Police Department, Detroit saw a 90% decrease in violent crime as well as a total stop to home invasions and murders in specific neighborhoods. Total homes served by TMC in Detroit [now number over 1,000, and businesses over 500][independent_uk_article]. 

# What Remains

# Sources

## References

* [Official Website][official_website]
* [Donation Page][official_website_financial_support] on Official Website
* [Independent UK article][independent_uk_article] about Detroit 

## Additional Resources

* [BBB Rating][bbb_rating] (A+ as of December 2014)
* [CBS Detroit](http://detroit.cbslocal.com/2011/02/27/classes-teach-ocean-travel-safety/) article about Dale Brown's advice for people captured by Somali pirates.
* [Huffington Post article](http://www.huffingtonpost.com/2012/03/08/detroit-threat-management-documentary-film_n_1326814.html) about a documentary

## See Also

* [Interview on Rock Newman Show](https://www.youtube.com/watch?v=8fT9YU6Pqj4)
[Huffington_Post_article]: http://www.huffingtonpost.com/2012/03/08/detroit-threat-management-documentary-film_n_1326814.html
[official_website]: http://www.threatmanagementcenter.com/
[bbb_rating]: http://www.bbb.org/detroit/business-reviews/self-defense/threat-management-center-in-detroit-mi-90004125/
[independent_uk_article]: http://www.independent.co.uk/news/world/americas/nothing-works-here-reality-on-the-streets-of-a-broken-motor-city-8721302.html
[official_website_financial_support]: http://www.threatmanagementcenter.com/fnclsprt.htm
