<!-- title: American Prairie Reserve and Wild Sky Beef -->

[American Prairie Reserve][site_home] is an organization dedicated to preserving wildlife in Montana. This is an area where president Franklin Roosevelt had [tried and failed][explore_wild_sky] to restore the bison population. [American Prairie Reserve's method][site_faq] is a mix of land ownership and easements [funded philanthropically][explore_wild_sky], and a [program that encourages][explore_wild_sky] cattle ranchers to act as conservationists.

American Prairie Reserve [originally started][explore_wild_sky] acquiring land using philanthropic funds. This approach had its limits, as it was causing some tension with neighboring cattle ranchers. They decided to [copy a model][explore_wild_sky] used in Namibia for Cheetah conservation (See: [Cheetah Conservation Fund][itsnotgov_cheetah_conservation_fund]). They started their [own line of beef][site_wild_sky] known as Wild Sky and paid their vendor cattle ranchers to act as conservationists on their own land. Ranchers get paid [specific rewards][explore_wild_sky] for various different conservation efforts they can undertake in the midst of their cattle ranching activities.

# Results

## Acres Protected

They have still [succeeded in obtaining][site_building] ownership of 91,588 acres of land as of 2018. The Wild Sky program [protects an additional][explore_wild_sky] 75,000 acres as of 2017.

## Funding

American Prairie Reserve's website [describes the organization][site_faq] as a "private nonprofit". However there is no clear statement about how much, if any, government grants or similar they receive. See their [financials page][site_financials] for details.

[comment]: <> (TODO: Donors info? See references in wikipedia article. It seems like it's legit but I don't understand what Wikipedia is citing here.)

## Government Involvement

The [ultimate goal][site_faq] of American Prairie Reserve is to obtain ownership of 500,000 acres to "glue together" a continuous 3.5 million acre wildlife reserve comprising federal, state, and their own privately owned land. They [plan to maintain][site_faq] ownership of the land they acquire in order to maintain "checks and balances" between them and the governments. Ultimately, absent the existence of the government owned reserves, this ultimate payoff of a giant combined reserve would not exist. The question remains as to how much investment would exist if they were creating the reserve completely on their own.

They have also [created a proposal][site_proposal] for federal and state governments to allow bison more room to graze on public lands.

As of 2018 [they lease][site_building] 307,791 acres of state and federal land in addition to the amount they own outright.

And finally, as is generally the case with conservation efforts, government is used to maintain property titles, and likely protection.

# Sources

## References

* American Prairie Reserve website:
[site_home]: https://www.americanprairie.org/
    * [Home page][site_home]
[site_faq]: https://www.americanprairie.org/building-the-reserve-faqs
    * [FAQ][site_faq]
[site_proposal]: https://www.americanprairie.org/news-blog/support-apr-grazing-proposal
    * [Proposal to let bison graze on public land][site_proposal]
[site_financials]: https://www.americanprairie.org/financials
    * [Financials][site_financials]
[site_wild_sky]: https://www.americanprairie.org/wild-sky-beef
    * [About Wild Sky][site_wild_sky]
[site_building]: https://www.americanprairie.org/building-the-reserve
    * [Information about Progress][site_building]

[explore_wild_sky]: https://explorepartsunknown.com/montana/conservation-montana-wild-sky/
* [Story of American Prairie and Wild Sky][explore_wild_sky] on Parts Unknown

## Other Sources

[wild_sky]: http://wildskybeef.org/about
* [Wild Sky brand][wild_sky] About page
[wikipedia]:https://en.wikipedia.org/wiki/American_Prairie_Reserve
* [Wikipedia page][wikipedia]

## See Also

[itsnotgov_cheetah_conservation_fund]: /infrastructure/land-resource-management/cheetah-conservation-fund
* [Cheetah Conservation Fund][itsnotgov_cheetah_conservation_fund] - Dogs save Cheetahs from extinction
