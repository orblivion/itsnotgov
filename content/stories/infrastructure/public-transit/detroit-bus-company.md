<!-- title: The Detroit Bus Company -->

The [Detroit Bus Company][site] is a company founded in Detroit by Andy Didorosi. It was founded in the midst of Detroit's financial troubles that eventually lead to its [bankruptcy](https://en.wikipedia.org/wiki/Detroit_bankruptcy), which lead to [cutbacks][cnbc] in their public transportation system. It [started off][cnbc] serving commuters in under-served areas of the city. Eventually it moved on to [providing tours][cnbc] of the city and [providing free rides][cnbc] to children attending after-school programs.

# Funding

Didorosi bought his initial bus with [$2800 of his own money][cnbc]. Eventually he [received $200,000][cnbc] from the Skillman Foundation (which appears to be [privately funded][skillman_history]).
It appears that the after-school program has been [sustained][cnbc] by money from the tour business via their ["Ride for Ride"][site_ride_for_ride] program, though it may be good to check on the financial details.

# Government Involvement

The bus company entered into a "[Public/private partnership][huffpo]" with Skillman Foundation in regard to its after-school program. Skillman [spent $100,000][huffpo] to develop software to aid in pickups and drop-offs of students. (It's unclear whether this relates to the aforementioned $200,000 from the same organization).

Again, Skillman Foundation appears to be a [privately funded][skillman_history], the "public" part of this partnership probably referring to the school district.

# Sources

## References

* [Official Website][site]
* [Official Website - "Ride for Ride" Program][site_ride_for_ride]
* [Huffington Post article][huffpo]
* [CNBC article][cnbc]
* [History of the Skillman Foundation][skillman_history]

## Further Viewing

* [Andy Didorosi: What Detroit Taught Me about Getting Things Done][talk]

[site]: https://thedetroitbus.com/
[site_ride_for_ride]: https://thedetroitbus.com/announcing-ride-for-ride/
[huffpo]: https://www.huffingtonpost.com/2013/08/22/detroit-bus-company_n_3791802.html
[cnbc]: https://www.cnbc.com/2016/10/11/how-a-college-dropout-took-on-detroits-500-million-public-transport-problem.html
[skillman_history]: https://www.skillman.org/our-founder/
[talk]: https://www.youtube.com/watch?v=aILsKzOEme0
