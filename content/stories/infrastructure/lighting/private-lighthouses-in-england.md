<!-- title: Private Lighthouses in England -->

<!-- redirect: /w/c/Infrastructure/Lighting/PrivateLighthousesInEngland -->

<!-- stub: Primary sources need to be pulled out of the economic papers, and laid out in this article. The opinions of the economists should be made of secondary importance, though any facts highlighted by Coase's critics should be among the facts listed in the article. -->

Economist Coase, in his paper [The Lighthouse In Economics][coase_paper] shows that, contrary to the claims that it is impossilbe, there were privately financed lighthouses in England's history.

Economist Bertrand, in her paper [The Coasean analysis of lighthouse financing: myths and realities][bertrand_paper], and economist Van Zandt, in his paper [The Lessons of the Lighthouse: "Government" or "Private" Provision of Goods][van_zandt_paper], make some counter arguments. 

# Sources

## Other Resources

* [The Lighthouse In Economics][coase_paper] by R. H. Coase
    * [Wikipedia page](http://en.wikipedia.org/wiki/The_Lighthouse_in_Economics) about this paper
* [The Coasean analysis of lighthouse financing: myths and realities][bertrand_paper] by Elodie Bertrand
* [The Lessons of the Lighthouse: "Government" or "Private" Provision of Goods][van_zandt_paper] by David E. Van Zandt

[coase_paper]: http://www.jstor.org/discover/10.2307/724895?sid=21106220037123&uid=4&uid=2
[bertrand_paper]: http://cje.oxfordjournals.org/content/30/3/389
[van_zandt_paper]: http://www.jstor.org/discover/10.2307/3085633?sid=21106220037123&uid=2129&uid=70&uid=4&uid=2
