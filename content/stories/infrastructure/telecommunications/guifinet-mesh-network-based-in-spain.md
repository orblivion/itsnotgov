<!-- title: Guifi.net - Mesh network based in Spain -->

<!-- redirect: /w/c/Infrastructure/Telecommunications/GuifinetMeshNetworkBasedInSpain -->

<!-- stub: Details require citation. Needs to be determined how the local municipalities are involved. -->

Guifi.net is a network, largely wireless, based in Spain. It is community owned, however has struck some deals with local municipalities in running fiberoptic lines.

# Sources

## Other Resources

* [Wikipedia article](http://en.wikipedia.org/wiki/Guifi.net)
* [Rising Voices article](https://rising.globalvoicesonline.org/blog/2013/12/11/guifi-net-spains-wildly-successful-diy-wireless-network/) which mentions deals with local municipalities.
* [European Commission article](http://ec.europa.eu/digital-agenda/en/guifinet-fftf-bottom-broadband-initiative) submitted by Guifi.net, wherein they mention that they receive cooperation from local governments, but not grants.
