<!-- title: Athens Wireless Metropolitan Network - Mesh network based in Greece -->

<!-- redirect: /w/c/Infrastructure/Telecommunications/AthensWirelessMetropolitanNetworkMeshNetwork -->

<!-- stub: Details require citations. -->

Athens Wireless Metropolitan Network (AWMN) is a community based mesh network in Athens, Greece. It was built in response to problems with broadband wireless in the area.

# Sources

## Reference

* [Official Website](http://www.awmn.net/content.php)

## Other Resources

* [Wikipedia article](http://en.wikipedia.org/wiki/Athens_Wireless_Metropolitan_Network)
