<!-- title: NYC Mesh -->

[NYC Mesh][site] is a community-based [mesh network][wikipedia_mesh] based in in New York City. It promises to provide a [network neutral][site_faq] connection, in spite of (and perhaps in response to) changes in FCC regulations that [took effect][cnet_neutrality] in 2018 to end the Obama-era FCC requirement that ISPs abide by network neutrality. It has a [direct connection][scientificamerican] to an Internet backbone, bypassing any traditional ISPs. As of December 2018, there are [255 active nodes][site_map].

# Funding

Joining the network [requires payment][site_faq] for parts and labor of installation. Most of the equipment is thus [owned by][site_faq] individual members. However it [receives funding][site_faq] from the of the [Internet Society][wikipedia_internet_society] New York Chapter, which accepts recommended donations from NYC Mesh members. 

The Internet Society is a non-profit, and it is not totally clear whether they receive any government funding. The [2015 financial statement][internet_society_financials_2015] from the parent organization doesn't seem to mention any. It [has received funding][techcrunch] from Google.org. That said, it is an outgrowth of the Internet Engineering Task Force, [which was][internet_society_history] a government funded project until 1998. [It appears][oreilly] that they were created in part to secure funding for the Internet Engineering Task Force after government funding ended. Its [membership consists][internet_society_members] almost entirely of companies, and each level of [membership requires][internet_society_membership_levels] a monetary contribution.

# Sources

## References

* NYC Mesh Website
[site]: https://www.nycmesh.net
    * [Site home][site]
[site_map]: https://www.nycmesh.net/map/
    * [Current network map][site_map]
[site_faq]: https://www.nycmesh.net/faq/
    * [Frequently Asked Questions][site_faq]
[scientificamerican]: https://www.scientificamerican.com/article/net-neutrality-loss-could-rekindle-isp-alternatives-for-internet-access/
* [Scientific American article][scientificamerican] about alternatives to traditional ISPs which mentions NYC Mesh
[cnet_neutrality]: https://www.cnet.com/news/the-net-neutrality-fight-isnt-over-heres-what-you-need-to-know/
* [CNET Article][cnet_neutrality] about Network Neutrality
[techcrunch]: https://techcrunch.com/2013/02/25/google-org-provides-4-4m-in-grants-to-the-internet-society-and-nsrc-to-improve-internet-access-in-sub-saharan-africa/
* [TechCrunch Article][techcrunch] about Google.org donating to the Internet Society
* Internet Society
[internet_society_members]: https://www.internetsociety.org/about-internet-society/organization-members/ (Internet Society - Members)
    * [Members][internet_society_members]
[internet_society_membership_levels]: https://www.internetsociety.org/about-internet-society/organization-members/membership-levels/ (Internet Society - Membership Levels)
    * [Membership Levels][internet_society_membership_levels]
[internet_society_financials_2015]: https://www.internetsociety.org/wp-content/uploads/2017/07/isoc_financials_web.pdf
    * [2015 Financial Statement][internet_society_financials_2015]
[internet_society_history]: https://www.internetsociety.org/internet/history-of-the-internet/ietf-internet-society/
    * [history][internet_society_history]
[oreilly]: https://www.oreilly.com/openbook/opensources/book/ietf.html (O'Reilly - The Internet Engineering Task Force)
* ["The Internet Engineering Task Force"][oreilly] - chapter from "Open Sources: Voices from the Open Source Revolution" - O'Reilly

## Other Sources

[wikipedia_mesh]: https://en.wikipedia.org/wiki/Mesh_networking
  * [Wikipedia article][wikipedia_mesh] about mesh networking
[wikipedia_internet_society]: https://en.wikipedia.org/wiki/Internet_Society
  * [Wikipedia article][wikipedia_internet_society] about the Internet Society
[mashable]: https://mashable.com/2018/01/09/mesh-networks-provide-alternative-intenet-connection/
  * [Mashable article][mashable] mentioning NYC Mesh

