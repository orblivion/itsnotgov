<!-- title: Independent groups create bike and walking lanes without government permission -->

<!-- redirect: /w/c/Infrastructure/Roads/independent_groups_bike_lanes -->

<!-- left-friendly: yes -->

<!-- stub: -->

# News

* http://grist.org/briefly/cyclists-and-walkers-are-building-their-own-bike-lanes-and-crosswalks/

# Twitter accounts of groups

* https://twitter.com/PBOTrans
* https://twitter.com/SFMTrA
