<!-- title: Roads -->

<!-- redirect: /w/c/Infrastructure/Roads -->

Who will build the roads? The age old question. Roads are often considered to be too expensive to be able to be funded by private organizations. However many early roads were privately funded. There are also some modern examples.

# See Also

* [Traffic Regulation](/w/c/Regulation/GroundTrafficRegulation/)