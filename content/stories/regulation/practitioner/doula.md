<!-- title: Doula Practice -->

[Doulas are][webmd] professionals, generally [not medically trained][wsj], who provide [physical and emotional][wsj] support to women throughout the childbirth process. There is [some tension][wsj] between doulas and medical professionals, when doulas opt to give contradictory medical advice. Despite this, there is some [research showing][webmd] that women who opt for doulas have improved outcomes associated with their pregnancy, such as a reduction in chance of needing a caesarean section, less need for pain medication, and more success breastfeeding.

As of 2016, doulas are [not regulated][slate] by the government. There are [private certifications][wsj] available, some of which prohibit contradicting advice of medical staff, and some which do not. As such, there [is some controversy][slate] among doulas regarding certification.

# Sources

## References

[slate]: https://slate.com/human-interest/2016/01/more-doulas-can-help-lower-the-cost-of-childbirth-there-s-just-one-problem.html
* [Opinion Piece][slate] on Slate.com in favor of regulating Doulas
[webmd]: https://www.webmd.com/baby/what-is-a-doula
* [WebMD article][webmd] about doulas.
[wsj]: https://web.archive.org/web/20160115191217/http://www.wsj.com/news/articles/SB107446888698004731
* [Wall Street Journal][wsj] article about the conflicts between doulas and medical staff.

## Other Sources

[scienceandsensibility]: https://www.scienceandsensibility.org/blog/op-ed-regulation-of-doulas---a-simple-solution-or-complex-conundrum
* [Opinion Piece][scienceandsensibility] opposed to regulating Doulas
[wikipedia]: https://en.wikipedia.org/wiki/Doula
* [Wikipedia page][wikipedia]

## Further Reading
[dona]: https://www.dona.org/the-dona-advantage/about/
* About page for [DONA International][dona], a doula certification organization
