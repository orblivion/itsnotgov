<!-- title: Insurance Institute for Highway Safety: Crash Testing Cars -->

The [Insurance Institute for Highway Safety][iihs_home], or IIHS, is a [nonprofit organization][iihs_about] started in 1959. They are funded entirely by a [large number][iihs_funding] of auto insurance companies and associations. The IIHS [performs crash tests][iihs_about] and provides safety ratings on automobiles. Autotrader.com [recommends considering][autotrader] IIHS safety ratings along side ratings from the [National Highway Traffic Safety Administration][nhtsa] when purchasing a car.

# Sources

## Reference

### IIHS Website

* [Main Website][iihs_home]
* [About Page][iihs_about]
* [Membership and Funding][iihs_funding]

### Autotrader

* [Recommendations for finding a vehicle][autotrader]

## Other Resources

* [Article on Business Insider][businessinsider] about how IIHS determines car safety ratings

## See Also

* [IIHS and safety regulation research][iihs_regulation_research_itsnotgov]

[iihs_home]: http://www.iihs.org/
[iihs_about]: https://www.iihs.org/iihs/about-us
[iihs_funding]: https://www.iihs.org/iihs/about-us/member-groups
[iihs_regulation_research_itsnotgov]: /regulation/ground-traffic-regulation/iihs-road-safety-regulation-research
[autotrader]: https://www.autotrader.com/car-info/crash-test-ratings-whats-difference-between-iihs-a-223740
[businessinsider]: https://www.businessinsider.com/iihs-how-car-safety-ratings-determined-top-pick-2018-6
[nhtsa]: https://en.wikipedia.org/wiki/National_Highway_Traffic_Safety_Administration
