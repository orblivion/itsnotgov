<!-- title: Consumer Reports -->

<!-- redirect: /w/c/Regulation/ProductSafety/ConsumerReports -->

<!-- stub: -->

[Consumer reports][website] is a consumer advocacy group, founded in 1936, which tests various products for quality. It has various internal policies that aim to maintain its reputation as an objective source of information. For instance, it has a ["No Commercial Use Policy"][no_commercial_use], which forbids companies from directly using a positive Consumer Reports review as part of an advertisement.

# Government support

It should be noted that their "no commercial use" policy requires government enforcement of their trademark. This is relevant because, whether or not Consumer Reports desires to be objective, it allows for them to bolster their *reputation* for objectivity in a way that would not be available otherwise. Without such a reputation, they arguably may not have as much of an impact on the market. However, their decision to use trademark in this way still points to their desire to be objective in the first place.

# Sources

## References
* [Consumer Reports Website][website]

## Other Resources

* [Wikipedia](https://en.wikipedia.org/wiki/Consumer_Reports)
* [Consumer Reports About Page][about_page]

[website]: http://www.consumerreports.org
[about_page]: http://www.consumerreports.org/cro/about-us/index.htm
[no_commercial_use]: http://www.consumerreports.org/cro/about-us/no-commercial-use-policy/index.htm
