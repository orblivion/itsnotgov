<!-- title: Car Insurance: Regulating drivers with behavioral analytics -->

<!-- redirect: /w/c/Regulation/GroundTrafficRegulation/AutoInsuranceTelematics -->

Governments create traffic rules to regulate behavior of drivers in various ways, to encourage them to drive more safely. Can auto insurance companies perform some of the same regulatory functions?

A number of auto insurance companies have started using so-called [telematic](http://en.wikipedia.org/wiki/Telematics) devices to monitor driver behavior. These devices are installed in automobiles, and are used to track various aspects of the driver's behavior, and report it to the insurance company. The aim is to give safer drivers an opportunity to lower their premium by proving themselves, but it also has the residual effect of encouraging them to employ safer driving habits. Because of this benefit, many drivers elect to install the device, despite not being required by their insurance contract.

[TOC]

# What behavior is being regulated?

## Mileage and Time of Day

The more people drive, the more likely they are to get into an accident. Driving at night is also more likely to cause an accident. Insurance companies offer "pay as you drive" policies, which charge a premium based on the amount of driving, and time of day. This data is recorded and reported to the insurance company via the telematic devices.

## Hard brakes, high speeds

Certain driving habits, such as hard braking, hard cornering, and high speeds, have been determined by insurance companies to be associated with a higher rate of accident. Some insurance companies have started using telematics to monitor and record these behaviors. Drivers who prove that they don't exhibit these habits can get a lower premium as a result. Some devices also provide feedback for the driver, letting them know when they are driving unsafely, helping the drivers to train in better habits.

# Specific Examples

Here are some policies offered as of December 2014:

* ["Snapshot"](http://www.progressive.com/auto/snapshot/) from Progressive
    * Monitors distance, time of day, hard brakes
    * [more details](http://www.progressive.com/auto/snapshot-common-questions/)
* ["Drivewise"](http://www.allstate.com/drive-wise.aspx) from Allstate
    * Monitors speed, time of day, hard brakes
* ["Drive Safe & Save"](https://www.statefarm.com/insurance/auto/discounts/drive-safe-save) from State Farm
    * Monitors mileage, sometimes "basic driving characteristics"
* ["Low-Mileage Discount"](http://www.nationalgeneral.com/auto-insurance/smart-discounts/low-mileage-discount.asp) from National General
    * Monitors mileage

# Results

In analyzing results of these programs, we are specifically curious about improvement in driver behavior. This will tell us something about insurance companies taking on some of the government's role in driver regulation. However, in determining whether these programs are altogether worthwhile, we must consider all costs and benefits to all parties involved - drivers, insurance companies, bystanders, those in the general public who are concerned with the wellbeing of others, etc.

So it's important to ask both questions: Do these programs succeed in regulating drivers? Is this program a net benefit? Admittedly, the answer to the second question may be somewhat subjective.

## Successes

[Here is an account][joemanna_testimonial] of a blogger who tried the program. He generally has a negative reaction to the program. However, he concedes that, in his opinion, it has made him a better driver:

> Overall, the intent of the Snapshot program accomplished its stated mission. It helped me become a safer driver.

## Disadvantages

### Privacy

As mentioned in the above blog post, a driver must be willing to sacrifice some privacy in order to accept the device.

### Premiums can go up

Some people [get a higher rate][forbes_article] from using the device.

# Sources

## References

* [Forbes article][forbes_article]

## Opinion Statements

* [Blogger Joe Manna's Account][joemanna_testimonial]

## Other Resources

* [Wikipedia article](http://en.wikipedia.org/wiki/Usage-based_insurance)
* [National Association of Insurance Commissioners overview](http://www.naic.org/cipr_topics/topic_usage_based_insurance.htm)

[forbes_article]: http://www.forbes.com/sites/adamtanner/2013/08/14/data-monitoring-saves-some-people-money-on-car-insurance-but-some-will-pay-more/
[joemanna_testimonial]: https://blog.joemanna.com/progressive-snapshot-review/