<!-- title: Cornucopia Institute -->
<!-- redirect: /regulation/food-safety/cornucopia-institute -->

The [Cornucopia Institute][site_home] is a farming policy and watchdog group [located in][site_faq] Cornucopia, Wisconsin, [with the aim][site_about_us] of upholding standards of organic and related farming principles. Among its activities is the investigation of various foods for their "scorecard" system, including [egg][site_egg_scorecard] and [dairy][site_dairy_scorecard] for those concerned with with animal welfare. They [also include][site_hexane_bars] guides to help consumers interested in organic food avoid certain hard-to-spot ingredients.

# Results

## Funding

[According to][site_about_us] their website, in 2017 their sources of income were "Individual Giving", "Foundations", "Cooperatives and Business", "Interest and other revenue", and "In-kind Contributions". The words "government" and "grant" do not appear in their [2017 financial report][site_financial_2017]. Their "total unresticted[sic] revenue and other support" [in 2017][site_financial_2017] was about 1 million dollars.

## Information

[Cornucopia Institute's website][site_home] provides information on various aspects of the market for consumers interested in organic food.

### Ingredient warnings

Their site has information about how to avoid hexane in [nutrition bars][site_hexane_bars] and [meat alternatives][site_hexane_fake_meat]. They have been cited on this topic by [CBS News][cited_cbs_news], [Consumer Reports][cited_consumer_reports], and [Food Safety News][cited_food_safety_news]. They also have similar warnings for [carrageenan][site_carrageenan] and other chemicals.

### Animal treatment scorecards

The Cornucopia Institute's [egg][site_egg_scorecard] and [dairy][site_dairy_scorecard] scorecards each have (as of 2019) over 100 farms listed. Each listing gives information on Cornucopia's assessment of the farm's adherence to organic farming principles, including treatment of animals. They have been cited on this topic by [NPR][cited_npr].

### Evidence of cheating gathered

In 2014, the Cornucopia Institute [commissioned aerial photographs][npr_flyover] of 14 purportedly organic farms to provide evidence of non-compliance. They [published the photographs][site_flyover] on their website, though they also [filed official complaints][npr_flyover] with the United States Department of Agriculture (USDA) (which was perhaps their primary motivation).

# Government Involvement

The Cornucopia Institute are also involved in influencing government policy, such as [advocating][site_gmo] for laws and [suing the USDA][site_sue_usda]. They also file complaints against companies [such as Target][village_voice_complaints] or [Horizon Organic][orlando_sentinel] for improper labeling of organic foods, including complaints based on [aerial photography][npr_flyover] they collected.

# Sources

## References

* Cornucopia Institute Website:
[site_home]: https://www.cornucopia.org
    * [Home page][site_home]
[site_about_us]: https://www.cornucopia.org/about-us/
    * [About Us][site_about_us]
[site_faq]: https://www.cornucopia.org/faq/
    * [FAQ][site_faq]
[site_carrageenan]: https://www.cornucopia.org/carrageenan/
    * [Information about carrageenan][site_carrageenan]
[site_hexane_bars]: https://www.cornucopia.org/hexane-guides/hexane_guide_bars.html
    * [Information about hexane in nutrition bars][site_hexane_bars]
[site_hexane_fake_meat]: https://www.cornucopia.org/hexane-guides/hexane_guide_meat_alternatives.html
    * [Information about hexane in meat alternatives][site_hexane_fake_meat]
[site_egg_scorecard]: https://www.cornucopia.org/scorecard/eggs/
    * [Egg Scorecard][site_egg_scorecard]
[site_dairy_scorecard]: https://www.cornucopia.org/scorecard/dairy/
    * [Dairy Scorecard][site_dairy_scorecard]
[site_financial_2017]: https://www.cornucopia.org/wp-content/uploads/2018/10/Cornucopia_AuditedFinancialStatements_2017_Final.pdf
    * [2017 Financial statement][site_financial_2017]
[site_sue_usda]: https://www.cornucopia.org/2006/04/organic-watchdog-sues-usda/
    * [Describing a lawsuit against the USDA][site_sue_usda]
[site_flyover]: https://www.cornucopia.org/organic-factory-farm-investigation/
    * [Aerial photographs of purportedly organic farms][site_flyover]
[site_gmo]: https://www.cornucopia.org/gmo-voting-scorecard/
    * [GMO Voting Scorecard][site_gmo]

[village_voice_complaints]: https://www.villagevoice.com/2009/12/14/legal-complaint-filed-against-dean-foods-for-trying-to-pass-off-non-organic-soy-milk-as-organic/
* [Village Voice article][village_voice_complaints] about the Cornucopia Institute filing a complaint over mislabeled organic soy milk.
[orlando_sentinel]: http://articles.orlandosentinel.com/2006-08-17/business/ORGANIC17_1_organic-milk-organic-dairy-aurora-organic
* [Orlando Sentinel article][orlando_sentinel] about the Cornucopia Institute filing a complaint over mislabeled organic milk.
[npr_flyover]: https://www.npr.org/sections/thesalt/2014/12/12/370096634/aerial-photos-are-new-weapon-in-organic-civil-war
* [NPR Article][npr_flyover] about the Cornucopia Institute taking aerial photographs of purportedly organic farms and filing complaints with the USDA.

[cited_consumer_reports]: https://www.consumerreports.org/cro/baby-formula/buying-guide/index.htm
* [Baby Formula buying guide][cited_consumer_reports] from Consumer Reports
[cited_food_safety_news]: https://www.foodsafetynews.com/2013/06/how-does-the-organic-industry-regulate-processing-aids/
* [Food Safety News article][cited_food_safety_news] about regulation of processing aids
[cited_cbs_news]: https://www.cbsnews.com/news/soy-burger-with-a-side-of-toxin/
* [CBS News article][cited_cbs_news] about hexane in vegetarian burgers
[cited_npr]: https://www.npr.org/sections/thesalt/2014/12/23/370377902/farm-fresh-natural-eggs-not-always-what-they-re-cracked-up-to-be
* [NPR article][cited_npr] about standards for "natural" eggs

## Other Sources

[wikipedia]: https://en.wikipedia.org/wiki/Cornucopia_Institute
* [Wikipedia article][village_voice_watergate] about the Cornucopia Institute
[village_voice_watergate]: https://www.villagevoice.com/2012/05/18/is-your-definition-of-organic-the-same-as-the-usdas-cornucopia-institute-declares-an-organic-watergate/
* [Village Voice article][village_voice_watergate] about the Cornucopia Institute's opinion that the USDA definition of organic is too lax
