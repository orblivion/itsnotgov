<!-- title: Food Safety at Costco -->

<!-- redirect: /w/c/Regulation/FoodSafety/CostcoFoodSafety -->

<!-- stub: -->

[Costco Wholesale Corporation](http://en.wikipedia.org/wiki/Costco) has [food safety standards][costco_food_safety_standards] for their products that go beyond the government's standards.

# Sources

## References

* [Costco food safety standards][costco_food_safety_standards], on Aspirago (see "informational links" for details on their policy) 
    * [National Sanitation Foundation](https://en.wikipedia.org/wiki/NSF_International) mentions Aspirago's [involvement with Costco][aspirago_costco_proof].

[aspirago_costco_proof]: http://www.nsf.org/newsroom_pdf/aspirago_costco_case_study.pdf
[costco_food_safety_standards]: https://webapps.aspirago.com/aspiragoportal/costcofoodportal.jsp
