<!-- title: Consumer Lab (Food Safety) -->

<!-- redirect: /w/c/Regulation/FoodSafety/private_fda -->

<!-- stub: -->

Consumer Lab (https://www.consumerlab.com/) is like a private FDA. They test products and supplements and provide detailed reports.

# See Also:


[Consumer Lab (Product Safety)](/w/c/Regulation/ProductSafety/consumer_lab/)
