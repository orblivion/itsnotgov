<!-- title: Shriners Hospitals for Children -->

[Shriners Hospitals for Children][site_home] is a hospital that treats children with conditions such as burns, cleft palate, and spinal cord injuries. It was founded by [Shriners International][shriners_international_masons], a fraternal order founded in 1872 as a spin-off from Freemasonry. Their [first location][cnn] opened in 1922. As of 2018 they have [22 locations][site_locations] between the United States and Canada, each having facilities for a subset of the conditions the Shriners network specializes in treating. Until 2011, the hospital [had a policy][healthcarenews] of paying for care through their endowment and never charging families for treatment. After that point, however, for reasons outlined below, it is unclear whether they remained a notable example of affordable provision of medicine without government assistance.

In 2009, because of the economic downturn, Shriners Hospitals for Children [lost a lot of the value][cnn] in their endowment, and even considered shutting down six of their hospitals. They ultimately [voted against it][cnn]. Their Galveston, TX location also [temporarily shut down][cnn] that same year due to hurricane Ike. To adapt to these new difficulties, in addition to rising healthcare costs, they [began a policy][healthcarenews] in 2011 of accepting insurance but still [giving financial assistance][site_financial_assistance] to any families in need. From this point, judging from their financial statements, the degree to which they cover expenses for needy families was comparable to the amount of money they received from the government. For instance, in their [2017 financial report][site_financials_2017], "Provision for Shriners assist" is listed as about $53 million, and "Other government revenue" is listed as about $34 million. Additionally, when Shriners Hospital started accepting insurance, it also began to accept government assistance such as Medicaid, though it's not clear whether this is considered part of the listed $34 million.

In their 2006 financial report, by contrast, there [was no mention][site_financials_2006] of government revenue. However it should be noted that their endowment [includes some][site_financials_2006] government securities.

# Sources

## References

* Shriners Website
[site_home]: https://www.shrinershospitalsforchildren.org/
    * [Home Page][site_home]
[site_locations]: https://www.shrinershospitalsforchildren.org/shc/locations
    * [Locations][site_locations]
[site_financial_assistance]: https://www.shrinershospitalsforchildren.org/shc/financial-assistance
    * [Financial Assistance][site_financial_assistance]
[site_financials_2006]: https://lovetotherescue.org/wp-content/uploads/2017/01/2006-combined-financial-statement.pdf
    * [2006 Financials][site_financials_2006]
[site_financials_2017]: https://lovetotherescue.org/wp-content/uploads/2018/04/2017-combined-financial-statement.pdf
    * [2017 Financials][site_financials_2017]
[cnn]: http://www.cnn.com/2009/HEALTH/07/07/shriners.hospitals/index.html
* [CNN story][cnn] about Shriners voting against closing hospitals
[healthcarenews]: https://healthcarenews.com/concession-to-reality-shriners-hospital-for-children-starts-accepting-payments/
* [Healthcare News story][healthcarenews] about the Shriners changing policy to accepting insurance
[shriners_international_masons]: https://www.shrinersinternational.org/Shriners/MasonShriners/Masons
* [Shriners International article][shriners_international_masons] explaining its relationship with Freemasonry

## Other Sources

[wikipedia]: https://en.wikipedia.org/wiki/Shriners_Hospitals_for_Children
* [Wikipedia Article][wikipedia]
[nytimes]: https://www.nytimes.com/2007/03/19/us/19shrine.html
* [New York Times article][nytimes] critiquing the financial practices of Shriners International. However it also gives more details on funding (which should probably be incorporated into this article text).
