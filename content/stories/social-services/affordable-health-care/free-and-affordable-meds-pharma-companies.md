<!-- title: Free and Affordable Medication Programs from Pharmaceutical Companies -->

<!-- redirect: /w/c/SocialServices/AffordableHealthCare/free_and_affordable_medication_programs_from_pharm -->

<!-- stub: -->

Some pharmaceutical companies have programs that offer some of their medication at a discounted rate, or even free of charge, on a financial need basis.

Some examples:

* https://www.astrazeneca-us.com/medicines/Affordability.html
* http://www.pfizerrxpathways.com/

Medicare website with a directory searchable by drug (listed here just as another source of info about the programs):

* https://www.medicare.gov/pharmaceutical-assistance-program/

TODO:

* Would be good to find evidence that there was no government pressure or incentive to provide these programs.
* Go through the Medicare site and look at all the links, start listing out programs. There seem to be a lot going on out there.
* If you see numbers, start listing them out. (income level, $ saved for consumers, # of needy consumers who get meds, etc. More importantly, # of consumers who still don't)
* Tidy up this article, obviously.
