site:
	./makesite.py

serve: site
	cd _site && python3 -m http.server

# Assuming it's not already installed!
install-debian-deps:
	apt install python3-markdown python3-jinja2
